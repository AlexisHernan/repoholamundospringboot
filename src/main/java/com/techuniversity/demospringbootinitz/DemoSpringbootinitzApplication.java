package com.techuniversity.demospringbootinitz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoSpringbootinitzApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoSpringbootinitzApplication.class, args);
	}

}
